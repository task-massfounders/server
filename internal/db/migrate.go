package db

import (
	"gitlab.com/task-massfounders/server/internal/usecase"
	_ "gorm.io/driver/postgres"
	"gorm.io/gorm"
)

// Migrate database schemas
func Migrate(client *gorm.DB) error {
	err := client.AutoMigrate(
		&usecase.MassFounder{},
	)
	if err != nil {
		return err
	}

	// seed
	// if err = client.Create(usecase.MassFounder{
	// 	ID:         strconv.Itoa(503200961346),
	// 	Name:       "ОЛЕГ",
	// 	Surname:    "ЛОБАНОВ",
	// 	Count:      11,
	// 	MiddleName: "ВАЛЕНТИНОВИЧ",
	// }).Error; err != nil {
	// 	log.Println("Seed error", err)
	// }

	return nil
}
