package db

import (
	"context"
	"fmt"
	"log"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"

	"go.uber.org/fx"

	"gitlab.com/task-massfounders/server/internal/config"
	"gitlab.com/task-massfounders/server/internal/usecase"
)

// Postgres db object
type Postgres struct {
	Conf   *config.Config
	Client *gorm.DB
}

// NewClient creates a new postgres client
func NewClient(lc fx.Lifecycle, conf *config.Config) (*gorm.DB, error) {
	connectionStr := fmt.Sprintf(
		"host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",
		conf.DB.Host,
		conf.DB.Port,
		conf.DB.User,
		conf.DB.Password,
		conf.DB.Name,
	)
	client, err := gorm.Open(postgres.Open(connectionStr), &gorm.Config{})

	if err != nil {
		log.Fatal(err)
		return nil, err
	}

	lc.Append(fx.Hook{
		OnStart: func(ctx context.Context) error {
			err := Migrate(client)
			if err != nil {
				return err
			}
			return nil
		},
	})

	return client, nil
}

// InitPostgres initializes DB repository
func InitPostgres(client *gorm.DB, conf *config.Config) usecase.MassfounderRepository {
	return &Postgres{
		Conf:   conf,
		Client: client,
	}
}

// Find massfounder by ID
func (db *Postgres) Find(id string) (*usecase.MassFounder, error) {
	MassFounder := &usecase.MassFounder{}
	r := db.Client.Where("id = ?", id).First(MassFounder)
	if r.Error != nil {
		return nil, r.Error
	}
	return MassFounder, nil
}

// Update massfounders
func (db *Postgres) Update(massfounders []*usecase.MassFounder) error {
	// Create or Update massfounders
	r := db.Client.Delete(massfounders)
	if r.Error != nil {
		return r.Error
	}

	if err := db.Client.Create(&massfounders).Error; err != nil {
		return err
	}

	return nil
}
