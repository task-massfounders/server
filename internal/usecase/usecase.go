package usecase

import (
	"errors"
	"log"
	"strconv"

	"gitlab.com/task-massfounders/server/internal/config"
	"gitlab.com/task-massfounders/server/internal/utils"
	"gorm.io/gorm"
)

type MassFounder struct {
	ID         string `json:"id" gorm:"primaryKey"`
	Name       string `json:"name"`
	Surname    string `json:"surname"`
	MiddleName string `json:"middle_name"`
	Count      int    `json:"count"`
}

type MassfounderRepository interface {
	Find(id string) (*MassFounder, error)
	Update(massfounders []*MassFounder) error
}

type MassfounderUsecase interface {
	Check(id string) (*Result, error)
	Update() error
}

func InitMassfounderUsecase(conf *config.Config, repo MassfounderRepository) MassfounderUsecase {
	return &massfounderUsecase{
		Config: conf,
		Repo:   repo,
	}
}

type massfounderUsecase struct {
	Config *config.Config
	Repo   MassfounderRepository
}

type Result struct {
	Success bool `json:"success"`
	Count   int  `json:"count"`
}

func (u *massfounderUsecase) Check(id string) (*Result, error) {
	MassFounder, err := u.Repo.Find(id)
	if err != nil {
		log.Println("Cannot find massfounder:", err)
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return &Result{
				Success: true,
				Count:   0,
			}, nil
		}
		return nil, err
	}

	result := &Result{
		Success: false,
		Count:   MassFounder.Count,
	}
	return result, nil
}

func (u *massfounderUsecase) Update() error {
	csv, err := utils.ReadCSVFromUrl(u.Config.App.CsvURL)
	if err != nil {
		log.Println("Cannot read csv")
		return err
	}

	massFounders := make([]*MassFounder, 0)
	for i, row := range csv {
		// skip header
		if i == 0 || row[0] == "" {
			continue
		}

		count, err := strconv.Atoi(row[4])
		if err != nil {
			log.Println("cannot convert count to int")
		}

		massFounders = append(massFounders, &MassFounder{
			ID:         row[0],
			Surname:    row[1],
			Name:       row[2],
			MiddleName: row[3],
			Count:      count,
		})
	}

	if err = u.Repo.Update(massFounders); err != nil {
		return err
	}
	return nil
}
