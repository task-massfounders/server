package config

import (
	// "log"

	"log"
	"strings"

	"github.com/spf13/viper"
)

// Config is application configuration
type Config struct {
	DB  DatabaseConfig
	App App
}

type App struct {
	Port   string
	CsvURL string
}

// DatabaseConfig holds db config
type DatabaseConfig struct {
	Host     string
	Port     string
	Name     string
	User     string
	Password string
}

// InitConfig init config
func InitConfig(conf *Config) *Config {
	// Set config file
	viper.SetConfigName("config")
	viper.SetConfigType("yaml")
	viper.AddConfigPath("$HOME/Projects/tasks-massfounders/server/config")

	// Set env
	replacer := strings.NewReplacer(".", "_")
	viper.SetEnvPrefix("MASSFOUNDER")
	viper.SetEnvKeyReplacer(replacer)

	// Read config file
	if err := viper.ReadInConfig(); err != nil {
		if _, ok := err.(viper.ConfigFileNotFoundError); ok {
			log.Println("Config file not found")
		} else {
			log.Println("Config file parse error", err)
		}
	}
	// Read env and override config file
	viper.AutomaticEnv()

	// Set CLI flags
	if conf.DB.Host != "db_host" {
		viper.Set("db.host", conf.DB.Host)
	}
	if conf.DB.Port != "0" {
		viper.Set("db.port", conf.DB.Port)
	}
	if conf.App.Port != "0" {
		viper.Set("app.port", conf.App.Port)
	}

	config := &Config{
		DB: DatabaseConfig{
			Host:     viper.GetString("db.host"),
			Port:     viper.GetString("db.port"),
			Name:     viper.GetString("db.name"),
			User:     viper.GetString("db.user"),
			Password: viper.GetString("db.pass"),
		},
		App: App{
			Port:   viper.GetString("app.port"),
			CsvURL: viper.GetString("app.csv.url"),
		},
	}

	return config
}
