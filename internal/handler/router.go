package handler

import (
	"github.com/labstack/echo/v4"
)

// RegisterRoutes to the route group
func RegisterRoutes(rg *echo.Group, handler *Handler) {
	rg.GET("/check/:id", handler.Check)
	rg.GET("/update", handler.Update)
}
