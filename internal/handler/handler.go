package handler

import (
	"log"
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/task-massfounders/server/internal/usecase"
)

// Handler handles client requests
type Handler struct {
	Usecase usecase.MassfounderUsecase
}

// InitHandler instantiate the handler
func InitHandler(u usecase.MassfounderUsecase) *Handler {
	return &Handler{
		Usecase: u,
	}
}

// Check if massfounder exists
func (h *Handler) Check(c echo.Context) error {
	id := c.Param("id")
	result, err := h.Usecase.Check(id)
	if err != nil {
		log.Println("Cannot get MassFounder by id: ", id)
		return c.JSON(http.StatusInternalServerError, err)
	}
	return c.JSON(http.StatusOK, result)
}

func (h *Handler) Update(c echo.Context) error {
	if err := h.Usecase.Update(); err != nil {
		return c.JSON(http.StatusInternalServerError, err)
	}
	return c.JSON(http.StatusOK, "Updated")
}
