module gitlab.com/task-massfounders/server

go 1.14

require (
	github.com/labstack/echo/v4 v4.2.1
	github.com/lib/pq v1.3.0
	github.com/spf13/pflag v1.0.3
	github.com/spf13/viper v1.7.1
	github.com/thatisuday/commando v1.0.4 // indirect
	go.uber.org/fx v1.13.1
	gorm.io/driver/postgres v1.0.8
	gorm.io/gorm v1.21.6
)
