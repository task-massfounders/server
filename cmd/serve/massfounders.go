package main

import (
	"context"
	"fmt"
	"log"
	"strconv"

	"github.com/labstack/echo/v4"
	"gitlab.com/task-massfounders/server/internal/config"
	"gitlab.com/task-massfounders/server/internal/db"
	"gitlab.com/task-massfounders/server/internal/handler"
	"gitlab.com/task-massfounders/server/internal/usecase"
	"go.uber.org/fx"

	"github.com/thatisuday/commando"
)

// NewServer runs http server and listens to given port
func NewServer(lc fx.Lifecycle, conf *config.Config) *echo.Echo {
	e := echo.New()
	lc.Append(fx.Hook{
		OnStart: func(context.Context) error {
			port := conf.App.Port
			log.Print(fmt.Sprintf("Listening at port %s...", port))
			go e.Start(fmt.Sprintf(":%s", port))
			return nil
		},
		OnStop: func(ctx context.Context) error {
			log.Print("Server is shutting down")
			return e.Shutdown(ctx)
		},
	})

	return e
}

// RegisterRoutes to the echo engine
func registerRoutes(e *echo.Echo, h *handler.Handler) {
	api := e.Group("")
	// register endpoints
	handler.RegisterRoutes(api, h)
}

func updateRegistry(h usecase.MassfounderUsecase) {
	if err := h.Update(); err != nil {
		println("Update failed")
	}
}

func main() {
	commando.
		SetExecutableName("server").
		SetVersion("1.0.0").
		SetDescription("Update registry")

	commando.
		Register("serve").
		AddFlag("db_host", "Database host", commando.String, "db_host").
		AddFlag("db_port", "Database port", commando.Int, 0).
		AddFlag("port", "Server port", commando.Int, 0).
		SetAction(func(args map[string]commando.ArgValue, flags map[string]commando.FlagValue) {
			dbHost := flags["db_host"].Value.(string)
			dbPort := flags["db_port"].Value.(int)
			port := flags["port"].Value.(int)

			conf := &config.Config{
				DB: config.DatabaseConfig{
					Host: dbHost,
					Port: strconv.Itoa(dbPort),
				},
				App: config.App{
					Port: strconv.Itoa(port),
				},
			}
			configBuilder := func() *config.Config {
				return config.InitConfig(conf)
			}
			server := fx.New(
				fx.Provide(
					configBuilder,
					NewServer,
					handler.InitHandler,
					db.InitPostgres,
					db.NewClient,
					usecase.InitMassfounderUsecase,
				),
				fx.Invoke(registerRoutes),
			)
			server.Run()
		})
	commando.
		Register("update-registry").
		AddFlag("db_host", "Database host", commando.String, "db_host").
		AddFlag("db_port", "Database port", commando.Int, 0).
		AddFlag("port", "Server port", commando.Int, 0).
		SetAction(func(args map[string]commando.ArgValue, flags map[string]commando.FlagValue) {
			dbHost := flags["db_host"].Value.(string)
			dbPort := flags["db_port"].Value.(int)
			port := flags["port"].Value.(int)

			conf := &config.Config{
				DB: config.DatabaseConfig{
					Host: dbHost,
					Port: strconv.Itoa(dbPort),
				},
				App: config.App{
					Port: strconv.Itoa(port),
				},
			}
			configBuilder := func() *config.Config {
				return config.InitConfig(conf)
			}

			update := fx.New(
				fx.Provide(
					configBuilder,
					db.InitPostgres,
					db.NewClient,
					usecase.InitMassfounderUsecase,
				),
				fx.Invoke(updateRegistry),
			)
			update.Start(context.TODO())
		})
	commando.Parse(nil)
}
